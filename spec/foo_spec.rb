require "foo"

describe Foo do
  ENV['AMOUNT_OF_SUCCEEDING_TESTS'].to_i.times do |i|
    it "succeeding test #{i}" do
      expect(Foo.name).to eq('Foo')
    end
  end
end
